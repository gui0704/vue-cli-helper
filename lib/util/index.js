const path = require('path');
const fs = require('fs');
const ejs = require('ejs');

function compileEjsFile(template, pageName) {
  return new Promise((resolve, reject) => {
    const filePath = `../templates/${template}`;
    const fullPath = path.resolve(__dirname, filePath);
    ejs.renderFile(fullPath, { name: pageName }, {}, (err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  });
}

function syncCreateDir (pathName){
  if (fs.existsSync(pathName)) {
    return true;
  }
  if (syncCreateDir(path.dirname(pathName))) {
    fs.mkdirSync(pathName);
    return true;
  }
}


function writeToFile(pathName,content) {
  return fs.promises.writeFile(pathName, content);
}





module.exports = {
  compileEjsFile,
  syncCreateDir,
  writeToFile,
};
