#! /usr/bin/env  node

const helpOption = require('./lib/core/help')
const createCommand = require('./lib/core/create');


const program = require('commander');

// 版本
program.version(require('./package.json').version)

// 帮助信息
helpOption();

// 创建
createCommand();



program.parse(process.argv);




