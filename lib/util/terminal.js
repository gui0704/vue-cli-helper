const { spawn } = require('child_process');

const commandSpawn = (...args) => {
  const childProcess = spawn(...args);
  childProcess.stdout.pipe(process.stdout);
  childProcess.stderr.pipe(process.stderr);
};

module.exports = { commandSpawn };
