const program = require('commander');
const { createCommandAction, createPageAction } = require('./actions');

const createCommand = () => {
  // 创建项目
  program
    .command('create <project>')
    .description('create a new project')
    .action(createCommandAction);

  // 创建pages
  program
    .command('addPage <page>')
    .description('add page and router')
    .action((page) => {
      const options = program.opts();
      createPageAction(page, options.dest || 'src/pages');
    });
};

module.exports = createCommand;
